<?php

use App\Post;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 1000; $i++) {
            $post = new Post();
            $post->title = "title " . $i;
            $post->body = "body " . $i;
            $post->save();
        }
        // $this->call(UserSeeder::class);
    }
}
