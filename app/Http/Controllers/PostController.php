<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    public function allPosts(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'body',
            4 => 'id',
        );

        $totalData = Post::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = Post::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts =  Post::where('id', 'LIKE', "%{$search}%")
                ->orWhere('title', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Post::where('id', 'LIKE', "%{$search}%")
                ->orWhere('title', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $show =  route('posts.show', $post);
                $edit =  route('posts.edit', $post);
                $delete =  route('posts.destroy', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['title'] = $post->title;
                $nestedData['body'] = substr(strip_tags($post->body), 0, 50);
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' class='btn btn-primary' >SHOW</a>
                          &emsp;<a href='{$edit}' title='EDIT' class='btn btn-secondary'>EDIT</a>&emsp;
                          <form action='{$delete}' method='POST' style='display:inline'>
                          <input type='hidden' name='_token' id='csrf-token' value=" . Session::token() . " />
                          <input type='hidden' name='_method' value='delete'>
                          <input onclick='return confirm(`confirm ?`);' type='submit' value='Delete' class='btn btn-danger'>
                        </form>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function show(Post $post)
    {
        return view('show', compact('post'));
    }

    public function edit(Post $post)
    {
        return view('edit', compact('post'));
    }

    public function update(Request $request, Post $post)
    {
        // dd($post);
        $post->update([
            'title' => $request->title,
            'body' => $request->body,
        ]);
        return view('datatable3');
    }

    public function destroy($id)
    {
        Post::where('id', $id)->delete();
        return redirect()->back();
    }
}
