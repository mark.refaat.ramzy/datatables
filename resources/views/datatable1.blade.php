@extends('layouts.app')

@section('content')

<div class="container">
    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Column 1</th>
                <th>Column 2</th>
                <th>Column 3</th>
                <th>Column 4</th>
                <th>Column 5</th>
            </tr>
        </thead>
        <tbody>
            @for ($i = 0; $i < 100; $i++) <tr>
                <td>Row {{ $i }} Data 1</td>
                <td>Row {{ $i }} Data 2</td>
                <td>Row {{ $i }} Data 3</td>
                <td>Row {{ $i }} Data 4</td>
                <td>Row {{ $i }} Data 5</td>
                </tr>
                @endfor
        </tbody>
    </table>
</div>
<script>
    $(document).ready( function () {
        $('#datatable1').DataTable();
    });
</script>

@endsection