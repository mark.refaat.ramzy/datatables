@extends('layouts.app')

@section('content')

<div class="container">
    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <th>Id</th>
            <th>Title</th>
            <th>Body</th>
            <th>Options</th>
        </thead>
    </table>
</div>
<script>
    $(document).ready( function () {
        $('#datatable1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('allPosts') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "id" },
                { "data": "title" },
                { "data": "body" },
                { "data": "options" ,'searchable':false, 'orderable':false}
            ]	 
        });
    });
</script>

@endsection