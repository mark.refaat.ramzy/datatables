@extends('layouts.app')

@section('content')
<div class="card" style="width: 18rem;">
    <div class="card-header">
        {{ $post['id'] }} - {{ $post['title'] }}
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">{{ $post['body'] }}</li>
    </ul>
</div>
@endsection