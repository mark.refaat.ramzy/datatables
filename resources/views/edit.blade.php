@extends('layouts.app')

@section('content')
<form action="{{route('posts.update',$post)}}" method="post">
    @csrf
    @method('put')
    <div class="card" style="width: 18rem;">
        <div class="card-header">
            <input type="text" value=" {{ $post['title'] }}" name="title">
        </div>
        <ul class="list-group list-group-flush">
            <input type="text" value=" {{ $post['body'] }}" name="body" style="padding: 5px">
        </ul>
        <br>
        <input type="submit" value="Update" class="btn btn-primary">
    </div>
</form>
@endsection