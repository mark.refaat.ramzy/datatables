@extends('layouts.app')

@section('content')

<div class="container">
    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>username</th>
                <th>email</th>
            </tr>
        </thead>
    </table>
</div>
<script>
    $(document).ready( function () {
        $('#datatable1').DataTable({
            "ajax" : { "url": "http://jsonplaceholder.typicode.com/users", "dataSrc": ""},
            columns : [
            { "data" : "id"},
            { "data" : "name"},
            { "data" : "username"},
            { "data" : "email"}
            ]
        });
    });
</script>

@endsection