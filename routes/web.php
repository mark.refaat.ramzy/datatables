<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/datatable1', function () {
    return view('datatable1');
})->name('datatable1');
Route::get('/datatable2', function () {
    return view('datatable2');
})->name('datatable2');
Route::get('/datatable3', function () {
    return view('datatable3');
})->name('datatable3');
Route::post('/allPosts', 'PostController@allPosts')->name('allPosts');
Route::resource('posts', 'PostController');
